<h1>Spigot/CraftBukkit用 Tab補完プラグイン</h1>

**[ダウンロード Download](https://gitlab.com/kameme1208/kameTab/issues/)**

チャットでコマンドを入力中にTabキーを押すと、入力中の文字から自動的に候補を出してくれる機能を拡張するプラグイン。

どんなコマンドにも候補を割り込むことが可能なので、Tab補完に対応されていないプラグインのコマンドにも候補を割り当てることが出来ます。

その機能に加え、一部の特定の文字をコマンドブロックで使用する事が多い文字に置き換えてくれる機能も追加。

<h2>導入方法</h2>
|導入するソフトウェア|導入方法|
|--------------------|--------|
|Spigot|他のプラグインと同様にpluginsフォルダに入れてください|
|CraftBukkit|上と同じ|
|ForgeBukkit(Thermos/Cauldron等)|上と同じ|
|Sponge|対応未確認です|
|BungeeCord|BungeeCordのpluginsフォルダーとサーバー側のpluginsフォルダの両方に入れてください|
<h2>特定の文字からの変換機能</h2>
　kameTabには一部の特定文字で`Tab`を押すことにより自動変換してくれる機能があります。
 + **座標取得**<br>
    チャット中に`Tab`を押すことでプレイヤーの立っている場所の座標に変換されます。
  + **例**：@からスペースを空けずに`Tab`

```
  /tp name @
  ↓
  /tp name 100 56 23
```

----
 + **@セレクタ変換**<br>
   チャット中に`@p`や`@a`.`@e`などのセレクタを現在の座標を付けた状態に変換する。
  + **例**：`@e`からスペースを空けずに`Tab`

```
  /testfor @e
  ↓
  /testfor @e[x=121,y=75,z=8,r=33]
  
  　更にもう一度`Tab`を押すことでdx, dy, dzの形式になります
  /testfor @e[x=121,y=75,z=8,r=33]
  ↓
  /testfor @e[x=121,y=75,z=8,dx=-7,dy=-12,dz=30]
```

----
 + **アイテムスポイト機能**<br>
   手に持ったアイテム、目線の先のブロックのIDを取得できます。
  + `@b`：目線先にあるブロック
  + `@i`：手に持っているブロック
  + **例**：`@i`からスペースを開けずに`Tab`(手に金床を持っていた場合

```
  /give name @i
  ↓
  /give name minecraft:anvil
```

----
 + **アイテムNBTスポイト機能**<br>
   手に持ったアイテムの保存されているNBTを取得できます。
  + ※内部で変換されているものが戻ってくるので、コマンドを入力したときにつけたNBTとは、形が少し違う場合があります。
  + 何もNBTを持っていないアイテムの場合は`{display:{Name:"",Lore:[""]},ench:[{id:0,lvl:1}]}`が入力されます
  + ※サーバーの仕様上、カラーコードは読み飛ばされて変換されます。
  + **例**：`{`からスペースを開けずに`Tab`

```
  「/give @p minecraft:stick 1 0 {customnbt:"ねこねこ"}」と入力したアイテムでやった場合
  /give name minecraft:anvil 1 0 {
  ↓
  /give name minecraft:anvil 1 0 {customnbt:"ねこねこ"}
```

----
 + **テキスト補完機能**<br>
   `tellraw`,`title`とか1.9以降からJson形式で入力するようになってめんどくさいので簡易に。
  + **例**：`"`からスペースを開けずに`Tab`(手に金床を持っていた場合

```
  /tellraw @a "
  ↓
  /tellraw @a {"text":""}
```

----
<h2>カスタムTab補完の設定</h2>
 　kameTabにはプラグインで追加されたコマンドにTab補完が無い場合、上書きしたい場合などでConfigに書き込むことで簡単に追加することが出来ます。
 
 * Citizens2に候補を追加する
    Citizens2のコマンドのよく使う一部の有名なコマンド(/npc)に候補を追加してみます。

```
Commands:
  /npc:
    - "create >Player"    </npc create プレイヤー名となる候補を追加
    - "remove >Player"    </npc remove プレイヤー名となる候補を追加
```

　　　また、このように共通点があるコマンドであるのならば、

```
Commands:
  "/npc":
    - ">group1 >Player"
Groups:
  "group1":    <グループを作りまとめることが可能
    - "create"
    - "remove"
```
　　　と、先頭に`>`を付けることで、複数の候補をまとめることも可能です。

 * HolographicDisplaysに候補を追加

    なぜここまで違う意味の丁寧な候補があってTabキーで変換できないしと思ったHolographicDisplaysに候補を追加してみます。(言い方悪い)

```
Commands:
  "/hd":
    - ">edit hologramName"
    - "addline >Any text"  >Anyを入力するとそのブロックは何でも良い判定になる(テキストは追加されないため別で追加)
    - "removeline >Any >Number"
    - ">edit1 >Any >Number text"
    - ">edit2 >Any extension"
Groups:
  "edit":
    - create
    - delete
    - edit
    - addline
    - removeline
    - setline
    - insertline
    - readtext
    - readimage
    - into
  "edit1":
    - setline
    - insertline
  "edit2":
    - readtext
    - readimage
```
+ Config内で自動的に変換される文字

|変換前|変換後/例|説明|
|------|---------|----|
|>Player|プレイヤー名|サーバーにオンラインのプレイヤー|
|>OfflinePlayer|プレイヤー名|一度でもサーバーに来たことがあるプレイヤー|
|>ProxiedPlayer|プレイヤー名|BungeeCord経由の場合、他のサーバーも含めたオンラインのプレイヤー|
|>TargetBlock|minecraft:stone / STONE|視点先にあるブロック|
|>BukkitMaterial|STONE|大文字のアイテム名|
|>MinecraftMaterial|minecraft:stone|giveとかで使うアイテム名|
|>Number|0|数字|
|>PlayerX|100|プレイヤーのX座標|
|>PlayerY|100|プレイヤーのY座標|
|>PlayerZ|100|プレイヤーのZ座標|
|>TargetX|100|プレイヤーが見ているブロックのX座標|
|>TargetY|100|プレイヤーが見ているブロックのY座標|
|>TargetZ|100|プレイヤーが見ているブロックのZ座標|
|>Any|なし|どの文字でもよい|
----
<h2>Permissionの設定</h2>
 kameTabにはいくつかの権限があります。
 また、どのコマンドの候補を選択できるかは、パーミッションの設定で指定することが可能です。

|Permission|初期値|説明|
|----------|------|----|
|kametab.admin|OPのみ|アドミン用のコマンド実行権限|
|kametab.tab.loc|無効|`@`のTab変換|
|kametab.tab.select|無効|`@e``@a`などのTab変換|
|kametab.tab.block|無効|`@b`のTab変換|
|kametab.tab.item|無効|`@i`のTab変換|
|kametab.tab.tag|無効|`{`のTab変換|
|kametab.tab.text|無効|`"`のTab変換|
|kametab.default|有効|最初の1ブロック目でデフォルトの補完を許可するか|
|kametab.custom.\<Command...>|無効|追加したTab補完の権限を与える※|

----

* <h2>追加したTab補完の権限の設定例</h2>
  少し癖のあるかき方になってしまったため、細かく説明しようと思います。

  * **例**

    /example dog Wann
    
    /example dog kyun~

    /example dog Nyan
    
    /example cat Nyan
    
    /example cat Niss!
    
    この5つの候補があった場合`cat Nyan`の方のみ許可したい場合をPermissionにすると
    
```
    kametab.custom./example.cat.Nyan
```

　　　となります。`cat`の両方(`全て`)を許可したい場合は


```
    kametab.custom./example.cat.*
``` 

　　　2ブロック目を飛ばし(何かの名前だった場合とか)3ブロック目が`Nyan`の場合を許可したい場合は


```
    kametab.custom./example.*.Nyan
``` 

　　　と`*`を付ければその場所は`すべての権限`が許可されます。

　　　また、上のように権限を設定するとその途中にあるコマンドの候補 /example catもTab補完で許可されることになります。

　　　(1ブロック目の/exampleはコマンドに登録されている場合のみデフォルトの仕様で出ます)
